const express = require('express');
const router = express.Router();
const artists = require('../controllers/artists.js');
const multer = require('multer');
const fs = require('fs');



// Config directory for artist's images
function createDirectorio(path) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
};
const storageArtistImage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(createDirectorio('./uploads/images/artists_images/'),
        './uploads/images/artists_images/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "A" + file.originalname);
    }
});

const uploadArtistImage = multer({
    storage: storageArtistImage
});

//Routes for Artists
router.route('/artists/:id?')
.get(artists.get)
.post(uploadArtistImage.single('image'), artists.post)
.put(artists.put)
.delete(artists.delete);

//Routes for Songs
router.route('/songs/:id?').get(artists.findSongs);

router.route('/songs/caratula/:id?').get(artists.findImage);

router.route('/songs/ringtone/:id?/:name?').get(artists.ringtone)

router.route('/songs/info/:id?').get(artists.findSong);
//Routes for Videos
//Routes for Audios
//Routes for Images

module.exports = router;
