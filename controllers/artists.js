
const path = require("path");
const directorio = 'D:/DBM';
const artists = require('../db_apis/artists.js');
const fs = require('fs');


function get(req, res, next){
    const context = {
        id: parseInt(req.params.id),
        offset: parseInt(req.query.offset),
        limit: parseInt(req.query.limit),
        order: req.query.order
    };

    artists.find(context)
    .then(rows=> {
        if(req.params.id){
            if(rows.length === 1){
                res.status(200).json(rows[0]);
            } else {
                res.status(404).end();
            }
        } else{
            res.status(200).json(rows);
        }
    })
    .catch(next);
 
    
}
 


module.exports.get = get;


//Canciones por artista

function findSongs(req, res, next){
    const context = {
        id: parseInt(req.params.id),
        offset: parseInt(req.query.offset),
        limit: parseInt(req.query.limit),
        order: req.query.order
    };

    artists.findSongs(context)
    .then(rows=> {
        if(req.params.id){
            if(rows.length === 1){
                fs.readFile(`D:/DBM/`+ rows[0].img_name, (err, data)=>{
        
                    //error handle
                    if(err) res.status(500).send(err);
                    
                    //get image file extension name
                    let extensionName = path.extname(`D:/DBM/`+ rows[0].img_name);
                    
                    //convert image file to base64-encoded string
                    let base64Image = new Buffer(data, 'binary').toString('base64');
                    
                    //combine all strings
                    let imgSrcString = `data:image/${extensionName.split('.').pop()};base64,${base64Image}`;
                    
                    //send image src string into jade compiler
                    rows[0].base64 =imgSrcString;
                    res.status(200).json(rows);
                })
                

             
        } else{
            res.status(200).json(rows);
                }        }
    })

    .catch(next);
 
    
}
module.exports.findSongs = findSongs;

function ringtone(req, res, next){
   
       var id= parseInt(req.params.id);
       var send = {codigo: id};
       console.log(id);
       var name = req.params.name;
        var route  ="";

    if(fs.existsSync(`D:/DBM/ring_tone_`+name)){
        route=`D:/DBM/ring_tone_`+name;
        fs.readFile(route, (err, data)=>{
        
            //error handle
            if(err) res.status(500).send(err);
            
            //get image file extension name
            let extensionName = path.extname(route);
            
            //convert image file to base64-encoded string
            let base64Image = new Buffer(data, 'binary').toString('base64');
            
            //combine all strings
            let imgSrcString = `data:audio/${extensionName.split('.').pop()};base64,${base64Image}`;
            
            //send image src string into jade compiler
            
            res.status(200).json(imgSrcString);
        })
    }else{
        artists.ringtone(send)
    .then(artist => {
        route=`D:/DBM/ring_tone_`+name;
        fs.readFile(route, (err, data)=>{
        
            //error handle
            if(err) res.status(500).send(err);
            
            //get image file extension name
            let extensionName = path.extname(route);
            
            //convert image file to base64-encoded string
            let base64Image = new Buffer(data, 'binary').toString('base64');
            
            //combine all strings
            let imgSrcString = `data:audio/${extensionName.split('.').pop()};base64,${base64Image}`;
            
            //send image src string into jade compiler
            
            res.status(200).json(imgSrcString);
    }) })
    .catch(next);

    }
   
               

             
               }
   


    module.exports.ringtone = ringtone;


function findImage(req, res, next){
    const context = {
        id: parseInt(req.params.id),
        offset: parseInt(req.query.offset),
        limit: parseInt(req.query.limit),
        order: req.query.order
    };

    artists.findImage(context)
    .then(rows=> {
        if(req.params.id){
            if(rows.length === 1){  
                fs.readFile(`D:/DBM/`+ rows[0].img_name, (err, data)=>{
        
                    //error handle
                    if(err) res.status(500).send(err);
                    
                    //get image file extension name
                    let extensionName = path.extname(`D:/DBM/`+ rows[0].img_name);
                    
                    //convert image file to base64-encoded string
                    let base64Image = new Buffer(data, 'binary').toString('base64');
                    
                    //combine all strings
                    let imgSrcString = `data:image/${extensionName.split('.').pop()};base64,${base64Image}`;
                    
                    //send image src string into jade compiler
                    rows[0].base64 =imgSrcString;
                    res.status(200).json(rows[0]);
                })
                

             
        } else{
            res.status(200).json(rows);
                }        }
    })

    .catch(next);
 
    
}
module.exports.findImage = findImage;

function findSong(req, res, next){
    const context = {
        id: parseInt(req.params.id),
        offset: parseInt(req.query.offset),
        limit: parseInt(req.query.limit),
        order: req.query.order
    };

    artists.findSong(context)
    .then(rows=> {
        console.log(rows[0])
        res.status(200).json(rows[0])

    })

    .catch(next);
 
    
}
module.exports.findSong = findSong;

function post(req, res, next){
    const artist = {
        artist_name: req.body.artistName,
        genre: req.body.genre,
        biografy: req.body.biografy,
        img_name: req.file.filename
                
    }

    artists.create(artist)
    .then(artist => res.status(201).json(artist))
    .catch(next);
}

module.exports.post = post;

function put(req, res, next){
    const artist = {
        artist_cod: parseInt(req.params.id),
        artist_name: req.body.artist_name,
        genre: req.body.genre,
        biografy: req.body.biografy,
        image: req.body.image
    }

    artists.update(artist)
    .then(artist => res.status(200).json(artist))
    .catch(next);
}

module.exports.put = put;

function del(req, res, next){
    const id = parseInt(req.params.id)

    artists.delete(id)
    .then(artist => res.status(204).json())
    .catch(next);
}

module.exports.delete = del;