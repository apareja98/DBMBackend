
const oracledb = require('oracledb');
const database = require('../services/database.js');
const connectionName = 'musicStore';

const baseQuery =
    `SELECT ARTIST_COD "artistCod",
    ARTIST_NAME "artistName",
    GENRE "genre",
    BIOGRAFY "biografy",
    IMG_BASE64 "base64"
FROM ARTISTS`;

const paginator = '\n' +
    `OFFSET :offset ROWS
FETCH NEXT :fetch ROWS ONLY`;


async function find(context) {
    let query;
    const binds = {};
    const orderParts = (context.order || 'ARTIST_COD:ASC').split(':');
    let orderCol = orderParts[0];
    const orderDir = orderParts[1];

    if (orderCol === 'id') {
        orderCol = 'ARTIST_COD';
    }

    if (context.id) {
        binds.artist_cod = context.id;

        query = baseQuery + `\nWHERE ARTIST_COD = :artist_cod`;// +
        //+ `\nORDER BY ${orderCol} ${orderDir}`+
        // paginator;
    } else {
        binds.offset = context.offset || 0;
        binds.fetch = context.limit || 100;
        query = baseQuery +
            `\nORDER BY ${orderCol} ${orderDir}` +
            paginator;
    }

    console.log(query);

    const result = await database.simpleExecute(connectionName, query, binds, { fetchInfo: { "biografy": { type: oracledb.STRING } } });

    return result.rows;
}

module.exports.find = find;



async function findSongs(context) {
    let query=   `SELECT s.name_song "name_song", 
    s.song_cod "song_cod",
    s.lyrics "lyrics",
    a.img_name "img_name",
    a.album_cod "album_cod",
    s.song_name "song_name",
    a.album_name "album_name"
FROM SONGS s, album a`;
    var binds = {};
    var orderParts = (context.order || 's.SONG_COD:ASC').split(':');
    let orderCol = orderParts[0];
    var orderDir = orderParts[1];

    if (orderCol === 'id') {
        orderCol = 's.SONG_COD';
    }

    if (context.id) {
        binds.artist_cod = context.id;

        query = query + `\nWHERE s.album_cod = a.album_cod and a.ARTIST_COD = :artist_cod`;// +
        //+ `\nORDER BY ${orderCol} ${orderDir}`+
        // paginator;
    } else {
        binds.offset = context.offset || 0;
        binds.fetch = context.limit || 100;
        query = baseQuery +
            `\nORDER BY ${orderCol} ${orderDir}` +
            paginator;
    }

    console.log(query);

    const result = await database.simpleExecute(connectionName, query, binds, { fetchInfo: { "lyrics": { type: oracledb.STRING } } });

    
    return result.rows;
}

module.exports.findSongs = findSongs;


async function findImage(context) {
    let consulta=   `SELECT  a.img_name "img_name" FROM album a`;
    var binds = {};
    var orderParts = (context.order || 'a.album_cod:ASC').split(':');
    let orderCol = orderParts[0];
    var orderDir = orderParts[1];

    if (orderCol === 'id') {
        orderCol = 'a.album_cod';
    }

    if (context.id) {
        binds.artist_cod = context.id;

        consulta = consulta  + `\nWHERE a.album_cod =:artist_cod`;// +
        //+ `\nORDER BY ${orderCol} ${orderDir}`+
        // paginator;
    }
    

    console.log(consulta);

    const result = await database.simpleExecute(connectionName, consulta, binds);

    
    return result.rows;
}

async function findSong(context) {
    let consulta=   `SELECT s.name_song "nombre", s.lyrics "letra" from songs s`;
    var binds = {};
    var orderParts = (context.order || 's.song_cod:ASC').split(':');
    let orderCol = orderParts[0];
    var orderDir = orderParts[1];

    if (orderCol === 'id') {
        orderCol = 's.song_cod';
    }

    if (context.id) {
        binds.artist_cod = context.id;

        consulta = consulta  + `\nWHERE s.song_cod =:artist_cod`;// +
        //+ `\nORDER BY ${orderCol} ${orderDir}`+
        // paginator;
    }
    

    console.log(consulta);

    const result = await database.simpleExecute(connectionName, consulta, binds, { fetchInfo: { "letra": { type: oracledb.STRING } } });

    
    return result.rows;
}

module.exports.findSong = findSong;


module.exports.findImage = findImage;


async function ringtone(id){
    let genSql =  `BEGIN   generarringtone(:codigo);END; `;
    const result = await database.simpleExecute(connectionName, genSql, id);


    //artist.artist_cod = result.outBinds.artist_cod[0];

    return id;
}
module.exports.ringtone = ringtone;
const createSql =
    `BEGIN Insertar_ImagenBD(:artist_name, :genre, :biografy, :img_name); END;`;

async function create(artist) {
    /*artist.artist_cod = {
        dir: oracledb.BIND_OUT,
        type: oracledb.NUMBER
    }*/

    const result = await database.simpleExecute(connectionName, createSql, artist);


    //artist.artist_cod = result.outBinds.artist_cod[0];

    return artist;
}

module.exports.create = create;

const updateSql =
    `UPDATE ARTISTS SET
        ARTIST_NAME = :artist_name,
        GENRE = :genre,
        BIOGRAFY = :biografy,
        IMAGE = :image
        WHERE ARTIST_COD = :artist_cod`;

async function update(artist) {

    const result = await database.simpleExecute(connectionName, updateSql, artist);

    return artist;
}

module.exports.update = update;

const deleteSql =
    `DELETE ARTISTS WHERE ARTIST_COD = :artist_cod`;

async function del(id) {

    const result = await database.simpleExecute(connectionName, deleteSql, { 'artist_cod': id });
   
}

module.exports.delete = del;

