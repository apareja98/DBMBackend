'use strict'
//const  http = require('http'); //sólo para el ejemplo1
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router.js');

let app;

function start(){
    return new Promise((resolve, reject)=>{
        let port = process.env.HTTP_PORT || 3000;
        app = express();

        app.use(bodyParser.urlencoded({extended: false}));
        /*/Will parse incoming JSON requests and revive ISO 8601 string to instances of Date.*/
        app.use(bodyParser.json());

        //Enable CORS since we want to allow the API to be consumed by domains
        //other than the one the API is hosted from.
        app.use(enableCORS);

        //Mount the router at /api so all routes start with /api
        app.use('/api', router);

        //Mount the unexpected error handler last
        app.use(handleUnexpectedError);

        app.listen(port, (err) => {
            if (err){
                reject(err);
                return;
            }
            console.log(`Server running at http://localhost:${port}/`);

            resolve();
        });

    });

}

module.exports.start = start;

function enableCORS(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT, POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();    
}

function handleUnexpectedError(err, req, res, next) {
    console.log('An unexpected error ocurred', err);

    res.status(500).send({message: 'An error has ocurred, please contact support if error persist'});
}